import pytest
from tests.test_my_account_tab.data import MyAccountTabData
from pages.sign_in_page import SignInPage
from tests.test_sign_in.data import SignInData
from pages.my_account_tab import MyAccount



@pytest.fixture
def my_account_tab(driver):
    sign_in_page = SignInPage(driver)
    main_page = sign_in_page.do_login(SignInData.USERNAME, SignInData.PASSWORD)
    my_account_tab = main_page.go_to_my_account_tab()

    return my_account_tab


def test_my_account_tab(my_account_tab):
    my_account_tab.change_first_name(MyAccountTabData.USER_STG_FIRST_NAME_INPUT)
    my_account_tab.change_last_name(MyAccountTabData.USER_STG_LAST_NAME_INPUT)
    my_account_tab.change_email(MyAccountTabData.USER_STG_EMAIL_INPUT)
    my_account_tab.change_phone_number(MyAccountTabData.USER_STG_PHONE_NUM_INPUT)

    assert my_account_tab.is_at()
    #.... 

